using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float healtAmount = 100f;

    public void TakeDamage(float damageAmoaunt)
    {
        healtAmount -= damageAmoaunt;

        if(healtAmount <= 0)
        {
            Destroy(gameObject);
        }
    }
}

