using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float deley = 0.2f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksEffect;
    [SerializeField] private float sparksLifetime = 0.1f;
    [SerializeField] private Ammo ammo;
    private bool isReadyToShoot = true;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && isReadyToShoot)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        if (ammo.IsNoAmmo()) return;
        ammo.ReduceAmmo();
        playMuzzleFlash();
        Raycasting();
        StartCoroutine(DelayCountdown(deley));
    }

    private void playMuzzleFlash()
    {
        muzzleFlashEffect.Play();
    }

    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        SummonSparks(hit.point);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(damage);
    }
    private IEnumerator DelayCountdown(float deley)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(deley);
        isReadyToShoot = true;
    }

    private void SummonSparks(Vector3 point)
    {
        GameObject sparks = Instantiate(sparksEffect, point, Quaternion.identity);
        Destroy(sparks, sparksLifetime);
    }
}
