using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public event Message PlayerDeath;

    [SerializeField] private float healtAmount = 15f;

    public void TakeDamage(float damageAmoaunt)
    {
        healtAmount -= damageAmoaunt;

        if (healtAmount <= 0)
        {
            Debug.Log("You did");
            PlayerDeath?.Invoke();
        }
    }
}