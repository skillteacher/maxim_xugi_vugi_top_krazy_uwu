using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnimyAttack : MonoBehaviour
{
    [SerializeField] private float damage = 25f;
    [SerializeField] private EnemyAnimation enemyAnimation;
    [SerializeField] private Transform target;

    private void Start()
    {
        target = TargetsForEnimy.Instance.GetTarget();
    }

    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += Attacktarget;
    }

    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= Attacktarget;
    }

    public void Attacktarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    }
}
