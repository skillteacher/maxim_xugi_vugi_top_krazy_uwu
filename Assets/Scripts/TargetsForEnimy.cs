using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetsForEnimy : MonoBehaviour
{
    private List<Transform> targets = new List<Transform>();
    public static TargetsForEnimy Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddTarget(Transform targetTransform)
    {
        targets.Clear();
        targets.Add(targetTransform);
    }

    public Transform GetTarget()
    {
        return targets[0];
    }
    
}
